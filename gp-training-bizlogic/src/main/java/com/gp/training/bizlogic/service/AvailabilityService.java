package com.gp.training.bizlogic.service;

import com.gp.training.bizlogic.api.model.OfferDTO;
import com.gp.training.bizlogic.params.AvailSearchParams;

import java.util.List;

public interface AvailabilityService {

    public List<OfferDTO> findOffers(AvailSearchParams params);

    OfferDTO get(long id);
}
