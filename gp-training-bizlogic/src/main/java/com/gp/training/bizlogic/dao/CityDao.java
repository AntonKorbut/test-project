package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.City;

public interface CityDao extends GenericDao<City, Long> {

}
