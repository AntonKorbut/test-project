package com.gp.training.bizlogic.rest;

import com.gp.training.bizlogic.api.model.HotelInfoDTO;
import com.gp.training.bizlogic.api.resource.HotelInfoResource;
import com.gp.training.bizlogic.dao.HotelInfoDao;
import com.gp.training.bizlogic.domain.HotelInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class HotelInfoResourceImpl implements HotelInfoResource {

    @Autowired
    private HotelInfoDao hotelInfoDao;

    @Override
    public HotelInfoDTO getHotelInfo(long hotelInfoId) {
        HotelInfo hotelInfo = hotelInfoDao.get(Long.valueOf(hotelInfoId));
        return convertHotelInfo2DTO(hotelInfo);
    }

    @Override
    public List<HotelInfoDTO> getHotelsInfo() {
        List<HotelInfo> hotelsInfo = hotelInfoDao.getAll();

        List<HotelInfoDTO> hotelInfoDtos = new ArrayList<>(hotelsInfo.size());
        for (HotelInfo hotelInfo :
                hotelsInfo) {
            HotelInfoDTO dto = convertHotelInfo2DTO(hotelInfo);
            hotelInfoDtos.add(dto);
        }

        return hotelInfoDtos;
        /*return hotelsInfo.stream().map(c -> convertHotelInfo2DTO(c)).collect(Collectors.toList());*/
    }

    private HotelInfoDTO convertHotelInfo2DTO(HotelInfo hotelInfo) {
        HotelInfoDTO dto = new HotelInfoDTO();
        if (hotelInfo != null) {
            dto.setId(hotelInfo.getId());
            dto.setName(hotelInfo.getName());
            dto.setCode(hotelInfo.getCode());
        }
        return dto;
    }
}
