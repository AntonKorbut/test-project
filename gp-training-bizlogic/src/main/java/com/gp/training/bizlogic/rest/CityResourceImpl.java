package com.gp.training.bizlogic.rest;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.model.CountryDTO;
import com.gp.training.bizlogic.api.resource.CityResource;
import com.gp.training.bizlogic.dao.CityDao;
import com.gp.training.bizlogic.domain.City;
import com.gp.training.bizlogic.domain.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CityResourceImpl implements CityResource {

    @Autowired
    private CityDao cityDao;

    @Override
    public CityDTO getCity(long cityId) {
        City city = cityDao.get(Long.valueOf(cityId));
        return convertCity2DTO(city);
    }

    @Override
    public List<CityDTO> getCities() {
        List<City> cities = cityDao.getAll();

        List<CityDTO> cityDtos = new ArrayList<>(cities.size());
        for (City city : cities) {
            CityDTO dto = convertCity2DTO(city);
            cityDtos.add(dto);
        }

        return cityDtos;

		/*return cities.stream().map(c -> convertCity2DTO(c)).collect(Collectors.toList());*/
    }

    private CityDTO convertCity2DTO(City city) {
        CityDTO dto = new CityDTO();
        if (city != null) {
            dto.setId(city.getId());
            dto.setName(city.getName());
            dto.setCode(city.getCode());

            if (city.getCountry() != null) {
                Country country = city.getCountry();
                CountryDTO countryDTO = new CountryDTO();
                countryDTO.setId(country.getId());
                countryDTO.setName(country.getName());
                countryDTO.setCode(country.getCode());
                dto.setCountry(countryDTO);
            }
        }
        return dto;
    }
}
