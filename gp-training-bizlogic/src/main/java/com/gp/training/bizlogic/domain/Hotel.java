package com.gp.training.bizlogic.domain;

import javax.persistence.*;

/**
 * Created by Antonio on 06.02.2016.
 */
@Entity
@Table(name = "hotel")
public class Hotel extends AbstractEntity {
    @Column(name = "name")
    private String name;
    @Column(name = "code")
    private String code;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "city_id")
    private City city;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
