package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.api.model.OfferDTO;
import com.gp.training.bizlogic.params.AvailSearchParams;

import java.util.List;

public interface AvailabilityDao {

    OfferDTO get(long id);

    List<OfferDTO> findOffers(AvailSearchParams params);


}
