package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.MealType;

/**
 * Created by Antonio on 22.01.2016.
 */
public interface MealTypeDao extends GenericDao<MealType, Long> {
}
