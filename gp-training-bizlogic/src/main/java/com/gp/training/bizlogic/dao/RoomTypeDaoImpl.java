package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.RoomType;
import org.springframework.stereotype.Repository;

/**
 * Created by Antonio on 22.01.2016.
 */
@Repository
public class RoomTypeDaoImpl extends GenericDaoImpl<RoomType, Long> implements RoomTypeDao {

    public RoomTypeDaoImpl() {
        super(RoomType.class);
    }
}
