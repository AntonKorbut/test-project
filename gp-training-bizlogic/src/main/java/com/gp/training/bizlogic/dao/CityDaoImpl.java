package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.City;
import org.springframework.stereotype.Repository;

@Repository
public class CityDaoImpl extends GenericDaoImpl<City, Long> implements CityDao {

    public CityDaoImpl() {
        super(City.class);
    }

}
