package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.HotelInfo;

public interface HotelInfoDao extends GenericDao<HotelInfo, Long> {

}
