package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.api.builders.BookingBuilder;
import com.gp.training.bizlogic.domain.Booking;

public interface BookingDao extends GenericDao<Booking, Long> {

    Booking createBooking(BookingBuilder bookingBuilder);


    //Booking updateBooking(Booking bookings);

}
