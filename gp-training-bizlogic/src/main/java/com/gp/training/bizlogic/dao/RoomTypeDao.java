package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.RoomType;

/**
 * Created by Antonio on 22.01.2016.
 */
public interface RoomTypeDao extends GenericDao<RoomType, Long> {
}
