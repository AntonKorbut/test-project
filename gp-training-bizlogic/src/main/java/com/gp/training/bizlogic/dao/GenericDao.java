package com.gp.training.bizlogic.dao;

import java.util.List;

public interface GenericDao<T, PK> {
	
	T get(PK key);
	
	List<T> getAll();



}
