package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.HotelInfo;
import org.springframework.stereotype.Repository;

@Repository
public class HotelInfoDaoImpl extends GenericDaoImpl<HotelInfo, Long> implements HotelInfoDao {

    public HotelInfoDaoImpl() {
        super(HotelInfo.class);
    }
}
