package com.gp.training.bizlogic.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "country")
public class Country extends AbstractEntity {

    @Column(name = "code", insertable = true, nullable = false, unique = true, updatable = true)
    private String code;
    @Column(name = "name", insertable = true, nullable = false, unique = true, updatable = true)
    private String name;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_id")
    private List<City> cities;

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
