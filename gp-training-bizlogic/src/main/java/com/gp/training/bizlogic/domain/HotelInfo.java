package com.gp.training.bizlogic.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "hotel_info")
public class HotelInfo extends AbstractEntity {


    @Column(name = "name", insertable = true, nullable = false, unique = true, updatable = true)
    private String name;

    @Column(name = "code", insertable = true, nullable = false, unique = true, updatable = true)
    private String code;

    @Column(name = "description", insertable = true, nullable = false, unique = true, updatable = true)
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
