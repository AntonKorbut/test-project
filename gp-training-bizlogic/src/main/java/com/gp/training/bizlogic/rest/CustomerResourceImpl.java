package com.gp.training.bizlogic.rest;

import com.gp.training.bizlogic.api.builders.CustomerBuilder;
import com.gp.training.bizlogic.api.model.CustomerDTO;
import com.gp.training.bizlogic.api.resource.CustomerResource;
import com.gp.training.bizlogic.dao.CustomerDao;
import com.gp.training.bizlogic.domain.Customer;
import com.gp.training.bizlogic.parser.ParserEntity2DTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 03.02.2016.
 */
@Component
public class CustomerResourceImpl implements CustomerResource {

    @Autowired
    private CustomerDao customerDao;


    @Override
    public CustomerDTO getCustomer(long customerId) {
        Customer customer = customerDao.get(Long.valueOf(customerId));
        return ParserEntity2DTO.parseEntity2DTO(customer);
    }


    @Override
    public List<CustomerDTO> createCustomers(List<CustomerBuilder> customerBuilders) {
        List<Customer> customerList = customerDao.createCustomers(customerBuilders);
        List<CustomerDTO> customerDTOs = new ArrayList<>(customerList.size());
        for (Customer customer : customerList) {
            customerDTOs.add(ParserEntity2DTO.parseEntity2DTO(customer));
        }
        return customerDTOs;
    }


    @Override
    public List<CustomerDTO> getCustomers() {
        List<Customer> customers = customerDao.getAll();

        List<CustomerDTO> customerDtos = new ArrayList<>(customers.size());
        for (Customer customer : customers) {
            CustomerDTO dto = convertCustomer2DTO(customer);
            customerDtos.add(dto);
        }
        return customerDtos;
        //return countries.stream().map(c -> convertCustomer2DTO(c)).collect(Collectors.toList());
    }


    private CustomerDTO convertCustomer2DTO(Customer customer) {
        CustomerDTO customerDTO = new CustomerDTO();
        if (customer != null) {
            customerDTO.setId(customer.getId());
            customerDTO.setName(customer.getName());
            customerDTO.setSurname(customer.getSurName());
            customerDTO.setBirthday(customer.getBirthday());
            customerDTO.setSexType((customer.getSexType()));
        }
        return customerDTO;
    }


    private Customer convertDTO2Customer(CustomerDTO customerDTO) {
        Customer customer = new Customer();
        if (customerDTO != null) {
            customer.setId(customerDTO.getId());
            customer.setName(customerDTO.getName());
            customer.setSurName(customerDTO.getSurname());
            customer.setBirthday(customerDTO.getBirthday());
            customer.setSexType(customerDTO.getSexType());
        }
        return customer;
    }

}
