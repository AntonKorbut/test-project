package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.MealType;
import org.springframework.stereotype.Repository;

/**
 * Created by Antonio on 22.01.2016.
 */
@Repository
public class MealTypeDaoImpl extends GenericDaoImpl<MealType, Long> implements MealTypeDao {

    public MealTypeDaoImpl() {
        super(MealType.class);
    }
}
