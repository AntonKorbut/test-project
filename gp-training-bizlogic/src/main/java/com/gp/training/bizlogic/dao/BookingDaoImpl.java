package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.api.builders.BookingBuilder;
import com.gp.training.bizlogic.domain.Booking;
import com.gp.training.bizlogic.domain.Offer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class BookingDaoImpl extends GenericDaoImpl<Booking, Long> implements BookingDao {


    public BookingDaoImpl() {
        super(Booking.class);
    }

    @Override
    @Transactional
    public Booking createBooking(BookingBuilder bookingBuilder) {
        Booking bookingEntity = new Booking();
        Offer offer = new Offer();
        bookingEntity.setDateBegin(bookingBuilder.getDateBegin());
        bookingEntity.setDateEnd(bookingBuilder.getDateEnd());
        offer.setId(bookingBuilder.getOfferId());
        bookingEntity.setOffer(offer);
        bookingEntity.setGuestsCount(bookingBuilder.getGuestCount());
        Booking booking = em.merge(bookingEntity);

        return booking;
    }

//    @Override
//    public Booking updateBooking(Booking bookings) {
//        return em.merge(bookings);
//    }


}
