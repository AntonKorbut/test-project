package com.gp.training.bizlogic.rest;

import com.gp.training.bizlogic.api.model.OfferDTO;
import com.gp.training.bizlogic.api.resource.OfferResource;
import com.gp.training.bizlogic.params.AvailSearchParams;
import com.gp.training.bizlogic.service.AvailabilityService;
import com.gp.training.bizlogic.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OfferResourceImpl implements OfferResource {

    @Autowired
    private AvailabilityService availabilityService;

    @Override
    public OfferDTO get(long id) {
        return availabilityService.get(id);
    }

    @Override
    public List<OfferDTO> findOffers(long cityId, int guestCount, String startDate, String endDate) {

        AvailSearchParams params = AvailSearchParams.builder()
                .cityId(cityId)
                .guestCount(guestCount)
                .startDate(DateUtils.convertISODateStringToDate(startDate))
                .endDate(DateUtils.convertISODateStringToDate(endDate))
                .build();

        List<OfferDTO> offers = availabilityService.findOffers(params);
        return offers;
    }

}
