package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.Offer;

/**
 * Created by Antonio on 03.02.2016.
 */
public interface OfferDao extends GenericDao<Offer, Long> {
}
