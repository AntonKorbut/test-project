package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.Country;
import org.springframework.stereotype.Repository;

@Repository
public class CountryDaoImpl extends GenericDaoImpl<Country, Long> implements CountryDao {

    public CountryDaoImpl() {
        super(Country.class);
    }
}
