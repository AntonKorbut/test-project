package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.api.builders.CustomerBuilder;
import com.gp.training.bizlogic.domain.Booking;
import com.gp.training.bizlogic.domain.Customer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 03.02.2016.
 */
@Repository
@Transactional
public class CustomerDaoImpl extends GenericDaoImpl<Customer, Long> implements CustomerDao {

    public CustomerDaoImpl() {
        super(Customer.class);
    }


    @Override
    @Transactional
    public List<Customer> createCustomers(List<CustomerBuilder> params) {
        List<Customer> customers = new ArrayList<>(params.size());
        for (CustomerBuilder param : params) {
            customers.add(em.merge(convertParams(param)));
        }

        return customers;
    }


    private Customer convertParams(CustomerBuilder param) {
        Customer customer = new Customer();
        customer.setName(param.getName());
        customer.setSurName(param.getSurname());
        customer.setSexType(param.getSexType());
        customer.setBirthday(param.getBirthday());
        Booking booking = em.find(Booking.class, param.getBookingId());
        booking.getCustomerList().add(customer);
        customer.getBookingList().add(booking);
        return customer;
    }

   /* @Override
    public Customer updateCustomer(Customer customer) {
        return em.merge(customer);
    }*/
}
