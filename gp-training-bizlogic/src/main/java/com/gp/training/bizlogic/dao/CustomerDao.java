package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.api.builders.CustomerBuilder;
import com.gp.training.bizlogic.domain.Customer;

import java.util.List;

/**
 * Created by Antonio on 03.02.2016.
 */
public interface CustomerDao extends GenericDao<Customer, Long> {

    List<Customer> createCustomers(List<CustomerBuilder> customerCreateParams);

    //Customer updateCustomer(Customer customer);


}
