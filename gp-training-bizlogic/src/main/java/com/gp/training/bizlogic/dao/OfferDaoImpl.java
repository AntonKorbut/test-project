package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.Offer;
import org.springframework.stereotype.Repository;

/**
 * Created by Antonio on 03.02.2016.
 */
@Repository
public class OfferDaoImpl extends GenericDaoImpl<Offer, Long> implements OfferDao{

    public OfferDaoImpl(){
        super(Offer.class);
    }
}
