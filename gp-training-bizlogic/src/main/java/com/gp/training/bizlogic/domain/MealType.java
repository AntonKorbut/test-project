package com.gp.training.bizlogic.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Antonio on 22.01.2016.
 */
@Entity
@Table(name = "meal_type")
public class MealType extends AbstractEntity {

    @Column(name = "name", insertable = true, nullable = false, unique = true, updatable = true)
    private String name;

    @Column(name = "code", insertable = true, nullable = false, unique = true, updatable = true)
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
