package com.gp.training.bizlogic.rest;

import com.gp.training.bizlogic.api.builders.BookingBuilder;
import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.resource.BookingResource;
import com.gp.training.bizlogic.dao.BookingDao;
import com.gp.training.bizlogic.domain.Booking;
import com.gp.training.bizlogic.parser.ParserEntity2DTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BookingResourceImpl implements BookingResource {

    @Autowired
    private BookingDao bookingDao;


    @Override
    public BookingDTO createBooking(BookingBuilder bookingBuilder) {
        Booking booking = bookingDao.createBooking(bookingBuilder);
        BookingDTO bookingDTO = new BookingDTO();
        bookingDTO.setId(booking.getId());
        return bookingDTO;
    }


    @Override
    public BookingDTO getBooking(long bookingId) {
        Booking booking = bookingDao.get(Long.valueOf(bookingId));
        BookingDTO dto = ParserEntity2DTO.parseEntity2DTO(booking);
        return dto;
    }

}
