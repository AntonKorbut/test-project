package com.gp.training.bizlogic.parser;

import com.gp.training.bizlogic.api.model.*;
import com.gp.training.bizlogic.domain.*;

/**
 * Created by Antonio on 05.02.2016.
 */
public class ParserEntity2DTO {


    public static CityDTO parseEntity2DTO(City city) {
        CityDTO cityDTO = new CityDTO();
        if (city != null) {
            if (city.getCode() != null) {
                cityDTO.setCode(city.getCode());
            }
            if (city.getName() != null) {
                cityDTO.setName(city.getName());
            }
            if (city.getId() != null) {
                cityDTO.setId(city.getId());
            }
            if (city.getCountry() != null) {
                cityDTO.setCountry(parseEntity2DTO(city.getCountry()));
            }
        }
        return cityDTO;
    }

    public static CountryDTO parseEntity2DTO(Country country) {
        CountryDTO countryDTO = new CountryDTO();
        if (country != null) {
            if (country.getId() != null) {
                countryDTO.setId(country.getId());
            }
            if (country.getCode() != null) {
                countryDTO.setCode(country.getCode());
            }
            if (country.getName() != null) {
                countryDTO.setName(country.getName());
            }
        }
        return countryDTO;
    }

    public static CustomerDTO parseEntity2DTO(Customer customer) {
        CustomerDTO customerDTO = new CustomerDTO();
        if (customer != null) {
            if (customer.getId() != null) {
                customerDTO.setId(customer.getId());
            }
            if (customer.getName() != null) {
                customerDTO.setName(customer.getName());
            }
            if (customer.getSurName() != null) {
                customerDTO.setSurname(customer.getSurName());
            }
            if (customer.getSexType() != null) {
                customerDTO.setSexType(customer.getSexType());
            }
            if (customer.getBirthday() != null) {
                customerDTO.setBirthday(customer.getBirthday());
            }
        }
        return customerDTO;
    }


    public static BookingDTO parseEntity2DTO(Booking booking) {
        BookingDTO bookingDTO = new BookingDTO();
        bookingDTO.setId(booking.getId());

        if (booking.getOffer() != null) {
            OfferDTO offer = new OfferDTO();
            offer.setPrice(booking.getOffer().getPrice());
            offer.setHotel(parseEntity2DTO(booking.getOffer().getHotel()));
            offer.setRoom(parseEntity2DTO(booking.getOffer().getRoomType()));
            offer.setOfferId(booking.getOffer().getId());
            bookingDTO.setOffer(offer);
        }

        if(booking.getCustomerList() != null) {
            for(Customer customer : booking.getCustomerList()) {
                bookingDTO.getCustomerDTOList().add(parseEntity2DTO(customer));
            }
        }
        if (booking.getDateBegin() != null) {
            bookingDTO.setDateBegin(booking.getDateBegin());
        }
        if (booking.getDateEnd() != null) {
            bookingDTO.setDateEnd(booking.getDateEnd());
        }
        bookingDTO.setGuestsCount(booking.getGuestsCount());
        return bookingDTO;
    }

    private static RoomTypeDTO parseEntity2DTO(RoomType roomType) {
        RoomTypeDTO roomTypeDTO = new RoomTypeDTO();
        if (roomType != null) {
            if (roomType.getId() != null) {
                roomTypeDTO.setId(roomType.getId());
            }
            if (roomType.getName() != null) {
                roomTypeDTO.setName(roomType.getName());
            }
            if (roomType.getCode() != null) {
                roomTypeDTO.setCode(roomType.getCode());
            }
        }
        return roomTypeDTO;
    }

    public static HotelDTO parseEntity2DTO(Hotel hotel) {
        HotelDTO hotelDTO = new HotelDTO();
        if (hotel != null) {
            if (hotel.getId() != null) {
                hotelDTO.setId(hotel.getId());
            }
            if (hotel.getCity() != null) {
                hotelDTO.setCity(parseEntity2DTO(hotel.getCity()));
            }
            if (hotel.getCode() != null) {
                hotelDTO.setCode(hotel.getCode());
            }
            if (hotel.getName() != null) {
                hotelDTO.setName(hotel.getName());
            }
        }
        return hotelDTO;
    }
    private ParserEntity2DTO() {
    }
}
