package com.gp.training.bizlogic.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Antonio on 03.02.2016.
 */
@Entity
@Table(name = "customer")
public class Customer extends AbstractEntity {

    @Column(name = "name", insertable = true, nullable = false, unique = true, updatable = true)
    private String name;

    @Column(name = "surname", insertable = true, nullable = false, unique = true, updatable = true)
    private String surName;

    @Column(name = "sex", insertable = true, nullable = false, unique = true, updatable = true)
    private String sexType;

    @Column(name = "birthday", insertable = true, nullable = false, unique = true, updatable = true)
    private Date birthday;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL}, mappedBy = "customerList")
    private List<Booking> bookingList = new ArrayList<>();


    public Customer() {
    }

    public String getSexType() {
        return sexType.toString();
    }

    public void setSexType(String sexType) {
        this.sexType = sexType.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }


    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public List<Booking> getBookingList() {
        return bookingList;
    }

    public void setBookingList(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }
}
