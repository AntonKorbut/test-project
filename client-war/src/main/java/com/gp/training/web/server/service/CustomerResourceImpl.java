package com.gp.training.web.server.service;

import com.gp.training.bizlogic.api.model.CustomerDTO;
import com.gp.training.bizlogic.api.builders.CustomerBuilder;
import com.gp.training.bizlogic.client.resource.CustomerService;
import com.gp.training.web.shared.model.CustomerProxy;
import com.gp.training.web.shared.service.CustomerResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 04.02.2016.
 */
@Service
public class CustomerResourceImpl implements CustomerResource {

    @Autowired
    private CustomerService customerService;

    @Override
    public List<CustomerProxy> createCustomers(List<CustomerProxy> customers) {
        List<CustomerBuilder> params = new ArrayList<>(customers.size());
        for (CustomerProxy proxy : customers) {
            CustomerBuilder param = new CustomerBuilder.Builder()
                    .bookingId(proxy.getBookingId())
                    .sexType(proxy.getSexType())
                    .name(proxy.getName())
                    .surname(proxy.getSurname())
                    .birthday(proxy.getBirthday())
                    .build();
            params.add(param);
        }
        List<CustomerDTO> dtos = customerService.createCustomers(params);
        customers.clear();
        for(CustomerDTO dto : dtos) {
            customers.add(ParserDTO2Proxy.parseDto2Proxy(dto));
        }

        return customers;
    }

    private CustomerProxy convertDto2Proxy(CustomerDTO dto) {
        CustomerProxy customerProxy = new CustomerProxy();
        customerProxy.setId(dto.getId().intValue());
        customerProxy.setName(dto.getName());
        customerProxy.setSurname(dto.getSurname());
        customerProxy.setSexType(dto.getSexType());
        customerProxy.setBirthday(dto.getBirthday());


        return customerProxy;
    }
}
