package com.gp.training.web.shared.model;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

import java.util.Date;

/**
 * Created by Antonio on 05.02.2016.
 */
@Portable
public class BookingBuilderProxy {
    private int id;
    private int offerId;
    private Date dateBegin;
    private Date dateEnd;
    private int guestCount;

    public BookingBuilderProxy() {}

    public BookingBuilderProxy(@MapsTo("id") int id,
                              @MapsTo("offerId") int offer,
                              @MapsTo("dateBegin") Date dateBegin,
                              @MapsTo("dateEnd") Date dateEnd,
                              @MapsTo("guestCount") int guestCount) {
        this.id = id;
        this.offerId = offer;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
        this.guestCount = guestCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getGuestCount() {
        return guestCount;
    }

    public void setGuestCount(int guestCount) {
        this.guestCount = guestCount;
    }
}
