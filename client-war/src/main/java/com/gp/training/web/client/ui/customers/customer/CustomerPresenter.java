package com.gp.training.web.client.ui.customers.customer;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.client.ui.common.Presenter;
import com.gp.training.web.shared.model.BookingBuilderProxy;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CustomerProxy;

import java.util.List;


public interface CustomerPresenter extends Presenter {

    void create(BookingBuilderProxy booking, Callback<BookingProxy, Void> bookingCallback);

    void createCustomers(List<CustomerProxy> customers);
}
