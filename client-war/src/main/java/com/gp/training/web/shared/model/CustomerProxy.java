package com.gp.training.web.shared.model;


import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

import java.util.Date;

/**
 * Created by Antonio on 04.02.2016.
 */
@Portable
public class CustomerProxy {

    private int id;
    private String name;
    private String surname;
    private String sexType;
    private Date birthday;
    private int bookingId;




    public CustomerProxy() {
    }

    public CustomerProxy(@MapsTo("id") int id,
                         @MapsTo("name") String name,
                         @MapsTo("surname") String surname,
                         @MapsTo("sexType") String sexType,
                         @MapsTo("birthday") Date birthday,
                         @MapsTo("bookingId") int bookingId) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.sexType = sexType;
        this.birthday = birthday;
        this.bookingId = bookingId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSexType() {
        return sexType;
    }

    public void setSexType(String sexType) {
        this.sexType = sexType;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }


}
