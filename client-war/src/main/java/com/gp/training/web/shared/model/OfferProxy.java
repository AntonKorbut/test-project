package com.gp.training.web.shared.model;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

import java.math.BigDecimal;

@Portable
public class OfferProxy {

    private int offerId;
    private BigDecimal price;
    private HotelProxy hotel;
    private RoomTypeProxy room;

    public OfferProxy() {

    }

    public OfferProxy(@MapsTo("offerId") int offerId,
                      @MapsTo("hotel") HotelProxy hotel,
                      @MapsTo("room") RoomTypeProxy room,
                      @MapsTo("price") BigDecimal price) {
        this.offerId = offerId;
		this.price = price;
        this.hotel = hotel;
        this.room = room;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

	public BigDecimal getPrice() {
        return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

    public HotelProxy getHotel() {
        return hotel;
    }

    public void setHotel(HotelProxy hotel) {
        this.hotel = hotel;
    }

    public RoomTypeProxy getRoom() {
        return room;
    }

    public void setRoom(RoomTypeProxy room) {
        this.room = room;
    }

}
