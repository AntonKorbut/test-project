package com.gp.training.web.client.ui.bookings;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.gp.training.web.client.params.PageParams;
import com.gp.training.web.client.ui.bookings.booking.BookingPresenter;
import com.gp.training.web.client.ui.bookings.booking.BookingPresenterImpl;
import com.gp.training.web.client.ui.common.GenericBaseView;
import com.gp.training.web.client.ui.common.PageBaseView;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CustomerProxy;
import com.gp.training.web.shared.model.HotelProxy;
import com.sksamuel.jqm4gwt.JQMDialog;
import com.sksamuel.jqm4gwt.JQMPage;
import com.sksamuel.jqm4gwt.button.JQMButton;
import com.sksamuel.jqm4gwt.form.elements.JQMText;
import com.sksamuel.jqm4gwt.html.Paragraph;
import com.sksamuel.jqm4gwt.list.JQMList;

public class BookingView extends GenericBaseView<BookingPresenter> implements PageBaseView {

    private static BookingViewUiBinder uiBinder = GWT.create(BookingViewUiBinder.class);
    private static JQMDialog dialog = null;
    @UiField
    protected JQMText hotel;
    @UiField
    protected JQMText roomType;
    @UiField
    protected JQMText price;
    @UiField
    protected JQMText dateBegin;
    @UiField
    protected JQMText dateEnd;
    @UiField
    protected JQMText country;
    @UiField
    protected JQMText city;
    @UiField
    protected JQMList customersList;
    @UiField
    protected JQMText id;
    @UiField
    protected JQMButton nextBtn;
    private PageParams pageParams;
    private Callback<BookingProxy, Void> bookingCallback;
    private BookingProxy booking;

    public BookingView() {
        initWidget(uiBinder.createAndBindUi(this));
        setPresenter(new BookingPresenterImpl());
        buildsCallbacks();
    }

    @Override
    public void loadPageParams(PageParams params) {
        pageParams = params != null ? params : new PageParams();
        id.setValue(String.valueOf(pageParams.getBookingId()));
        result();
    }

    @UiHandler("nextBtn")
    void onClick(ClickEvent event) {
        if (dialog == null) {
            dialog = new JQMDialog();
            dialog.setCorners(false);
            dialog.setHeader("Result ->");
            dialog.setDlgTransparent(true);
            dialog.setDlgCloseBtn(JQMPage.DlgCloseBtn.RIGHT);
            FlowPanel panel = new FlowPanel();
            Paragraph messageText = new Paragraph("Booking is getting!");
            panel.add(messageText);
            messageText.getElement().getStyle().setPadding(1, Style.Unit.EM);
            dialog.add(panel);
            panel = new FlowPanel();
            JQMButton b = new JQMButton("Close");
            b.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    dialog.closeDialog();
                }
            });
            panel.add(b);
            dialog.add(panel);

        }
        dialog.openDialog();
        result();

    }

    private int getHashId() {
        return Integer.valueOf(Window.Location.getHash().substring(1));
    }

    private void setHashId(int id) {
        Window.Location.assign(Window.Location.createUrlBuilder()
                .setHash(id + "").buildString());
    }

    private void result() {
        setHashId(Integer.parseInt(id.getValue()));
        int id = getHashId();
        pageParams.setBookingId(id);

        getPresenter().getBooking(pageParams.getBookingId(), bookingCallback);

    }

    private void buildsCallbacks() {
        bookingCallback = new Callback<BookingProxy, Void>() {
            @Override
            public void onFailure(Void aVoid) {
            }

            @Override
            public void onSuccess(BookingProxy bookingProxy) {
                booking = bookingProxy;
                customersList.clear();
                for (CustomerProxy customer : bookingProxy.getCustomerProxyList()) {
                    customersList.addItem("  Name: " + customer.getName() +
                            "  Surname " + customer.getSurname()
                            + "  Sex: " + customer.getSexType());
                    customersList.recreate();
                    customersList.refresh();
                }
                HotelProxy hotelProxy = booking.getOffer().getHotel();
                country.setValue(hotelProxy.getCityProxy().getCountry().getName());
                roomType.setValue(booking.getOffer().getRoom().getName());
                city.setValue(hotelProxy.getCityProxy().getName());
                hotel.setValue(hotelProxy.getName());

                dateBegin.setValue(booking.getDateEnd().toString());
                dateEnd.setValue(booking.getDateEnd().toString());
                price.setValue(bookingProxy.getOffer().getPrice().toString());


            }
        };
    }

    interface BookingViewUiBinder extends UiBinder<Widget, BookingView> {
    }


}
