package com.gp.training.web.client.ui.customers.customer;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.shared.model.BookingBuilderProxy;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CustomerProxy;
import com.gp.training.web.shared.service.BookingResource;
import com.gp.training.web.shared.service.CustomerResource;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import java.util.List;

/**
 * Created by Antonio on 04.02.2016.
 */
public class CustomerPresenterImpl implements CustomerPresenter {

    @Override
    public void create(BookingBuilderProxy booking, final Callback<BookingProxy, Void> originCallback) {
        RestClient.create(BookingResource.class, new RemoteCallback<BookingProxy>() {
            @Override
            public void callback(BookingProxy bookingProxy) {
                if (originCallback != null && bookingProxy != null) {
                    originCallback.onSuccess(bookingProxy);
                }
            }
        }).createBooking(booking);
    }

    @Override
    public void createCustomers(List<CustomerProxy> customers) {
        RestClient.create(CustomerResource.class, new RemoteCallback<List<CustomerProxy>>() {
            @Override
            public void callback(List<CustomerProxy> customerProxies) {
            }
        }).createCustomers(customers);
    }
}
