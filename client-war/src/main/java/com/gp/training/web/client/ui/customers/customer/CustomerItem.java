package com.gp.training.web.client.ui.customers.customer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.sksamuel.jqm4gwt.form.elements.JQMRadioset;
import com.sksamuel.jqm4gwt.form.elements.JQMText;
import com.sksamuel.jqm4gwt.plugins.datebox.JQMCalBox;

import javax.validation.constraints.NotNull;

public class CustomerItem extends Composite {

    private static CustomerItemUiBinder uiBinder = GWT.create(CustomerItemUiBinder.class);
    @UiField
    @NotNull
    public JQMText name;
    @UiField
    @NotNull
    public JQMText surname;
    @UiField
    @NotNull
    public JQMRadioset sexType;
    @UiField
    @NotNull
    public JQMCalBox birthday;

    public CustomerItem() {

        initWidget(uiBinder.createAndBindUi(this));
    }

    interface CustomerItemUiBinder extends UiBinder<Widget, CustomerItem> {
    }

}
