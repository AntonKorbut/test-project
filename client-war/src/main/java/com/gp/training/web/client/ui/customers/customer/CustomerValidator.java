package com.gp.training.web.client.ui.customers.customer;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.validation.client.AbstractGwtValidatorFactory;
import com.google.gwt.validation.client.GwtValidation;
import com.google.gwt.validation.client.impl.AbstractGwtValidator;
import com.sksamuel.jqm4gwt.form.validators.Validator;

/**
 * Created by Antonio on 06.02.2016.
 */
public final class CustomerValidator extends AbstractGwtValidatorFactory {


    @Override
    public AbstractGwtValidator createValidator() {
        return GWT.create(GwtValidator.class);
    }

    @GwtValidation(CustomerItem.class)
    public interface GwtValidator extends Validator {
    }
}
