package com.gp.training.web.client.ui.bookings.booking;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.shared.model.BookingProxy;

/**
 * Created by Antonio on 06.02.2016.
 */
public interface BookingPresenter {
    void getBooking(int bookingId, final Callback<BookingProxy, Void> bookingCallback);
}
