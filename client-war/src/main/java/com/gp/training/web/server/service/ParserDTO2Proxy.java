package com.gp.training.web.server.service;


import com.gp.training.bizlogic.api.model.*;
import com.gp.training.web.shared.model.*;

/**
 * Created by Antonio on 06.02.2016.
 */

public class ParserDTO2Proxy {
    private ParserDTO2Proxy() {
    }

    public static CityProxy parseDto2Proxy(CityDTO cityDTO) {
        CityProxy cityProxy = new CityProxy();
        if (cityDTO != null) {
            if (cityDTO.getId() != null) {
                cityProxy.setId(cityDTO.getId().intValue());
            }
            if (cityDTO.getCode() != null) {
                cityProxy.setCode(cityDTO.getCode());
            }
            if (cityDTO.getName() != null) {
                cityProxy.setName(cityDTO.getName());
            }
            if (cityDTO.getCountry() != null) {
                cityProxy.setCountry(parseDto2Proxy(cityDTO.getCountry()));
            }
        }
        return cityProxy;
    }

    public static RoomTypeProxy parseDto2Proxy(RoomTypeDTO roomDTO) {
        RoomTypeProxy roomProxy = new RoomTypeProxy();
        if (roomDTO != null) {
            if (roomDTO.getCode() != null) {
                roomProxy.setCode(roomDTO.getCode());
            }
            if (roomDTO.getName() != null) {
                roomProxy.setName(roomDTO.getName());
            }
            if (roomDTO.getId() != null) {
                roomProxy.setId(roomDTO.getId().intValue());
            }
        }
        return roomProxy;
    }
    public static OfferProxy parseDto2Proxy(OfferDTO offerDTO) {
        OfferProxy offerProxy = new OfferProxy();
        if (offerDTO != null) {
            offerProxy.setOfferId(offerDTO.getOfferId().intValue());
            offerProxy.setPrice(offerDTO.getPrice());
            HotelDTO hotelDTO = offerDTO.getHotel();
            HotelProxy hotelProxy = parseDto2Proxy(hotelDTO);

            offerProxy.setHotel(hotelProxy);

            RoomTypeProxy roomProxy = parseDto2Proxy(offerDTO.getRoom());
            offerProxy.setRoom(roomProxy);

        }
        return offerProxy;
    }

    public static CountryProxy parseDto2Proxy(CountryDTO countryDTO) {
        CountryProxy countryProxy = new CountryProxy();
        if (countryDTO != null) {
            if (countryDTO.getId() != null) {
                countryProxy.setId(countryDTO.getId().intValue());
            }
            if (countryDTO.getCode() != null) {
                countryProxy.setCode(countryDTO.getCode());
            }
            if (countryDTO.getName() != null) {
                countryProxy.setName(countryDTO.getName());
            }
        }
        return countryProxy;
    }

    public static HotelProxy parseDto2Proxy(HotelDTO hotelDTO) {
        HotelProxy hotelProxy = new HotelProxy();
        if (hotelDTO != null) {
            hotelProxy.setCode(hotelDTO.getCode());
            hotelProxy.setName(hotelDTO.getName());
            if (hotelDTO.getId() != null) {
                hotelProxy.setId(hotelDTO.getId().intValue());
            }
            if (hotelDTO.getCity() != null) {
                hotelProxy.setCityProxy(parseDto2Proxy(hotelDTO.getCity()));
            }
        }
        return hotelProxy;
    }

    public static CustomerProxy parseDto2Proxy(CustomerDTO dto) {
        CustomerProxy customerProxy = new CustomerProxy();
        if (dto != null) {
            if (dto.getId() != null) {
                customerProxy.setId(dto.getId().intValue());
            }
            customerProxy.setName(dto.getName());
            customerProxy.setSurname(dto.getSurname());
            customerProxy.setSexType(dto.getSexType());
            customerProxy.setBirthday(dto.getBirthday());
        }
        return customerProxy;
    }
    public static BookingProxy parseDto2Proxy(BookingDTO dto) {
        BookingProxy proxy = new BookingProxy();
        if (dto != null) {
            proxy.setId((int) dto.getId());
            proxy.setGuestsCount(dto.getGuestsCount());
            proxy.setOffer(parseDto2Proxy(dto.getOffer()));
            if (dto.getDateBegin() != null) {
                proxy.setDateBegin(dto.getDateBegin());
            }
            if (dto.getDateEnd() != null) {
                proxy.setDateEnd(dto.getDateEnd());
            }
            if (dto.getCustomerDTOList() != null) {
                for (CustomerDTO customer : dto.getCustomerDTOList()) {
                    proxy.getCustomerProxyList().add(parseDto2Proxy(customer));
                }
            }
        }
        return proxy;
    }
}
