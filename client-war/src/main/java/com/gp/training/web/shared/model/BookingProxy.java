package com.gp.training.web.shared.model;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Antonio on 04.02.2016.
 */
@Portable
public class BookingProxy {

    private int id;
    private Date dateBegin;
    private Date dateEnd;
    private int guestsCount;
    private OfferProxy offer;
    private List<CustomerProxy> customerProxyList = new ArrayList<>();



    public BookingProxy() {}

    public BookingProxy(@MapsTo("id") int id,
                        @MapsTo("offer") OfferProxy offer,
                        @MapsTo("dateBegin")  Date dateBegin,
                        @MapsTo("dateEnd") Date dateEnd,
                        @MapsTo("guestsCount") int guestsCount,
                        @MapsTo("customerProxyList") List<CustomerProxy> customerProxyList) {
//        this.customerProxyList = new ArrayList<>();
        this.customerProxyList = customerProxyList;
        this.id = id;
        this.offer = offer;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
        this.guestsCount = guestsCount;
    }

    public List<CustomerProxy> getCustomerProxyList() {
        return customerProxyList;
    }

    public void setCustomerProxyList(List<CustomerProxy> customerProxyList) {
        this.customerProxyList = customerProxyList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getGuestsCount() {
        return guestsCount;
    }

    public void setGuestsCount(int guestsCount) {
        this.guestsCount = guestsCount;
    }

    public OfferProxy getOffer() {
        return offer;
    }

    public void setOffer(OfferProxy offer) {
        this.offer = offer;
    }
}
