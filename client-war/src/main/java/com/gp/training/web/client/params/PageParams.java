package com.gp.training.web.client.params;

import com.gp.training.web.shared.model.CustomerProxy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PageParams {
	
	private int guestCount;
	private int offerId;
	private Date startDate;
	private Date endDate;
	private int bookingId;

	public int getGuestCount() {
		return guestCount;
	}
	public void setGuestCount(int guestCount) {
		this.guestCount = guestCount;
	}
	public int getOfferId() {
		return offerId;
	}
	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

}
