package com.gp.training.web.shared.service;

import com.gp.training.web.shared.model.BookingBuilderProxy;
import com.gp.training.web.shared.model.BookingProxy;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by Antonio on 04.02.2016.
 */
@Path("/bookings")
public interface BookingResource {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public BookingProxy createBooking(@RequestBody BookingBuilderProxy proxy);

//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    @Path("{bookingId}")
//    public BookingDTO  findBooking(@QueryParam("bookingId") long bookingId);

    @GET
    @Path("/get/{bookingId}")
    @Produces(MediaType.APPLICATION_JSON)
    public BookingProxy getBooking(@PathParam("bookingId") long bookingId);
}
