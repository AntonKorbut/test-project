package com.gp.training.web.shared.model;

import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class HotelProxy {
	
	private int id;
	private String name;
	private String code;
	private CityProxy cityProxy;
	public HotelProxy(){
		
	}

	public CityProxy getCityProxy() {
		return cityProxy;
	}

	public void setCityProxy(CityProxy cityProxy) {
		this.cityProxy = cityProxy;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}
