package com.gp.training.web.client.ui.customers;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;
import com.gp.training.web.client.app.AppContext;
import com.gp.training.web.client.app.PageToken;
import com.gp.training.web.client.params.PageParams;
import com.gp.training.web.client.ui.common.GenericBaseView;
import com.gp.training.web.client.ui.common.PageBaseView;
import com.gp.training.web.client.ui.customers.customer.CustomerItem;
import com.gp.training.web.client.ui.customers.customer.CustomerPresenter;
import com.gp.training.web.client.ui.customers.customer.CustomerPresenterImpl;
import com.gp.training.web.shared.model.BookingBuilderProxy;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CustomerProxy;
import com.sksamuel.jqm4gwt.Transition;
import com.sksamuel.jqm4gwt.button.JQMButton;
import com.sksamuel.jqm4gwt.list.JQMList;
import com.sksamuel.jqm4gwt.list.JQMListItem;

import java.util.ArrayList;
import java.util.List;

public class CustomersView extends GenericBaseView<CustomerPresenter> implements PageBaseView {

    private static CustomersViewUiBinder uiBinder = GWT.create(CustomersViewUiBinder.class);
    @UiField
    protected JQMButton nextBtn;
    @UiField
    protected JQMList guestList;
    private Callback<BookingProxy, Void> bookingCallback;
    private Transition transition = Transition.SLIDE;
    private PageParams pageParams;

    public CustomersView() {
        initWidget(uiBinder.createAndBindUi(this));
        setPresenter(new CustomerPresenterImpl());
        buildsCallbacks();
    }

    private void buildsCallbacks() {
        bookingCallback = new Callback<BookingProxy, Void>() {
            @Override
            public void onFailure(Void aVoid) {
            }

            @Override
            public void onSuccess(BookingProxy bookingProxy) {
                List<CustomerProxy> customers = new ArrayList<>();
                List<JQMListItem> listItemList = guestList.getItems();
                for (JQMListItem listItem : listItemList) {
                    int size = listItem.getWidgetCount();
                    for (int i = 0; i < size; i++) {
                        Widget widget = listItem.getWidget(i);
                        if (widget instanceof CustomerItem) {
                            CustomerItem customerItem = (CustomerItem) widget;
                            CustomerProxy customer = new CustomerProxy();
                            customer.setBookingId(bookingProxy.getId());
                            customer.setName(customerItem.name.getValue());
                            customer.setSurname(customerItem.surname.getValue());
                            customer.setSexType(customerItem.sexType.getValue());
                            customer.setBirthday(customerItem.birthday.getDate());
                            customers.add(customer);
                        }
                    }
                }
                pageParams.setBookingId(bookingProxy.getId());
                getPresenter().createCustomers(customers);

                AppContext.navigationService.next(PageToken.BOOKING, pageParams);
            }
        };
    }

    @Override
    public void loadPageParams(PageParams params) {
        pageParams = params != null ? params : new PageParams();
        recreateGuestsList();
    }

    private void recreateGuestsList() {
        guestList.clear();

        int guestCount = pageParams.getGuestCount();

        for (int i = 0; i < guestCount; i++) {
            JQMListItem listItem = new JQMListItem();
            listItem.setControlGroup(true, false);

            CustomerItem customer = new CustomerItem();
            listItem.addWidget(customer);

            guestList.appendItem(listItem);
        }

        guestList.recreate();
        guestList.refresh();
    }

    @UiHandler("nextBtn")
    void onNextClick(ClickEvent event) {
        BookingBuilderProxy proxy = new BookingBuilderProxy();
        proxy.setOfferId(pageParams.getOfferId());
        proxy.setDateBegin(pageParams.getStartDate());
        proxy.setDateEnd(pageParams.getEndDate());
        proxy.setGuestCount(pageParams.getGuestCount());

        getPresenter().create(proxy, bookingCallback);
    }

    interface CustomersViewUiBinder extends UiBinder<Widget, CustomersView> {
    }


}
