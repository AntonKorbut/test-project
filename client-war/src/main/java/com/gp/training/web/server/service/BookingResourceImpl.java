package com.gp.training.web.server.service;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.builders.BookingBuilder;
import com.gp.training.bizlogic.client.resource.BookingService;
import com.gp.training.web.shared.model.BookingBuilderProxy;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.service.BookingResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Antonio on 04.02.2016.
 */
@Service
public class BookingResourceImpl implements BookingResource {

    @Autowired
    private BookingService bookingService;



    @Override
    public BookingProxy createBooking( BookingBuilderProxy proxy) {
        BookingBuilder params = new BookingBuilder.Builder()
                .offerId(proxy.getOfferId())
                .dateBegin(proxy.getDateBegin())
                .dateEnd(proxy.getDateEnd())
                .guestCount(proxy.getGuestCount())
                .build();
        BookingDTO dto = bookingService.createBooking(params);
        BookingProxy booking = convertDto2Proxy(dto);
        return booking;
    }

    @Override
    public BookingProxy getBooking(long bookingId) {
        BookingDTO dto = bookingService.getBooking(bookingId);

        BookingProxy proxy = ParserDTO2Proxy.parseDto2Proxy(dto);
        return proxy;
    }


    private BookingProxy convertDto2Proxy(BookingDTO dto) {
        BookingProxy bookingProxy = new BookingProxy();
        bookingProxy.setId(Integer.valueOf(String.valueOf(dto.getId())));
        return bookingProxy;
    }


}
