package com.gp.training.web.shared.service;

import com.gp.training.bizlogic.api.model.CustomerDTO;
import com.gp.training.web.shared.model.CustomerProxy;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Antonio on 04.02.2016.
 */
@Path("/customer")
public interface CustomerResource {
//    @GET
//    @Path("{customerId}")
//    @Produces(MediaType.APPLICATION_JSON)
//    public CustomerDTO getCustomer(@PathParam("customerId") long customerId);
//
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<CustomerDTO> getCustomers();
//
//
//    @POST
//    @Path("/post/create/customer")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    public CustomerDTO createCustomer(CustomerDTO dto);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<CustomerProxy> createCustomers(@RequestBody List<CustomerProxy> customers);
}
