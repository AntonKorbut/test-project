package com.gp.training.bizlogic.api.model;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;

@XmlRootElement(name = "offer")
@XmlAccessorType(XmlAccessType.FIELD)
public class OfferDTO {

    @XmlAttribute(name = "offerId")
    private Long offerId;
    @XmlElement(name = "price")
    private BigDecimal price;
    @XmlElement(name = "hotel")
    private HotelDTO hotel;
    @XmlElement(name = "room")
    private RoomTypeDTO room;

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public HotelDTO getHotel() {
        return hotel;
    }

    public void setHotel(HotelDTO hotel) {
        this.hotel = hotel;
    }

    public RoomTypeDTO getRoom() {
        return room;
    }

    public void setRoom(RoomTypeDTO room) {
        this.room = room;
    }

}
