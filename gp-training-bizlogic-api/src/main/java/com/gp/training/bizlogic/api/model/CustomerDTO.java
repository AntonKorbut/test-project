package com.gp.training.bizlogic.api.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Antonio on 03.02.2016.
 */
@XmlRootElement(name = "customer")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerDTO {


    @XmlAttribute(name = "id")
    private Long id;

    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "surname")
    private String surname;

    @XmlElement(name = "sex")
    private String sexType;

    @XmlElement(name = "birthday")
    private Date birthday;

    @XmlElement(name = "bookings")
    private List<BookingDTO> bookingDTO = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSexType() {
        return sexType;
    }

    public void setSexType(String sexType) {
        this.sexType = sexType;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public List<BookingDTO> getBookingDTO() {
        return bookingDTO;
    }

    public void setBookingDTO(List<BookingDTO> bookingDTO) {
        this.bookingDTO = bookingDTO;
    }
}

