package com.gp.training.bizlogic.api.resource;

import com.gp.training.bizlogic.api.model.HotelInfoDTO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/hotelInfo")
public interface HotelInfoResource {

    @GET
    @Path("{hotelId}")
    @Produces(MediaType.APPLICATION_JSON)
    HotelInfoDTO getHotelInfo(@PathParam("hotelId") long hotelId);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<HotelInfoDTO> getHotelsInfo();
}
