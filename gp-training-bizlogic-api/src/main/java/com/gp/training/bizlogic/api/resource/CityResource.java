package com.gp.training.bizlogic.api.resource;

import com.gp.training.bizlogic.api.model.CityDTO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/city")
public interface CityResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{cityId}")
    CityDTO getCity(@PathParam("cityId") long cityId);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<CityDTO> getCities();
}
