package com.gp.training.bizlogic.api.resource;

import com.gp.training.bizlogic.api.model.CountryDTO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/country")
public interface CountryResource {

    @GET
    @Path("{countryId}")
    @Produces(MediaType.APPLICATION_JSON)
    CountryDTO getCountry(@PathParam("countryId") long countryId);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<CountryDTO> getCountries();
}
