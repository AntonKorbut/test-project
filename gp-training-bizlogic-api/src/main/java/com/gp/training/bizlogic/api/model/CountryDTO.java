package com.gp.training.bizlogic.api.model;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "country")
@XmlAccessorType(XmlAccessType.FIELD)
public class CountryDTO {

    @XmlAttribute(name = "id")
    private Long id;
    @XmlElement(name = "code")
    private String code;
    @XmlElement(name = "name")
    private String name;

    public CountryDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
