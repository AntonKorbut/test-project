package com.gp.training.bizlogic.api.resource;


import com.gp.training.bizlogic.api.builders.CustomerBuilder;
import com.gp.training.bizlogic.api.model.CustomerDTO;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Antonio on 03.02.2016.
 */
@Path("/customer")
public interface CustomerResource {

    @GET
    @Path("{customerId}")
    @Produces(MediaType.APPLICATION_JSON)
    CustomerDTO getCustomer(@PathParam("customerId") long customerId);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<CustomerDTO> getCustomers();


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    List<CustomerDTO> createCustomers(List<CustomerBuilder> customerBuilders);


}
