package com.gp.training.bizlogic.api.resource;

import com.gp.training.bizlogic.api.builders.BookingBuilder;
import com.gp.training.bizlogic.api.model.BookingDTO;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/bookings")
public interface BookingResource {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public BookingDTO createBooking(BookingBuilder bookingBuilder);


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get/{bookingId}")
    public BookingDTO getBooking(@PathParam("bookingId") long bookingId);


}
