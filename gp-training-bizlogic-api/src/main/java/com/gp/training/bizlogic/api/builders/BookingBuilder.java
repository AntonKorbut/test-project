package com.gp.training.bizlogic.api.builders;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "bookingBuilder")
@XmlAccessorType(XmlAccessType.FIELD)
public class BookingBuilder {
    @XmlElement(name = "offer_id")
    private long offerId;

    @XmlElement(name = "guests_count")
    private int guestCount;

    @XmlElement(name = "date_begin")
    private Date dateBegin;

    @XmlElement(name = "date_end")
    private Date dateEnd;

    public long getOfferId() {
        return offerId;
    }

    public int getGuestCount() {
        return guestCount;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public static class Builder {
        private long offerId;
        private int guestCount;
        private Date dateBegin;
        private Date dateEnd;

        public Builder offerId(long offerId) {
            this.offerId = offerId;
            return this;
        }

        public Builder guestCount(int guestCount) {
            this.guestCount = guestCount;
            return this;
        }

        public Builder dateBegin(Date dateBegin) {
            this.dateBegin = dateBegin;
            return this;
        }

        public Builder dateEnd(Date dateEnd) {
            this.dateEnd = dateEnd;
            return this;
        }

        public BookingBuilder build() {
            BookingBuilder params = new BookingBuilder();
            params.offerId = offerId;
            params.guestCount = guestCount;
            params.dateBegin = dateBegin;
            params.dateEnd = dateEnd;
            return params;
        }
    }
}
