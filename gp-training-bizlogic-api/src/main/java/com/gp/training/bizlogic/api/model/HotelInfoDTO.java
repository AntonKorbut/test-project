package com.gp.training.bizlogic.api.model;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "hotelInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class HotelInfoDTO {

    @XmlAttribute(name = "hotelId")
    private Long id;
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "hotelCode")
    private String code;
    @XmlElement(name = "description")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
