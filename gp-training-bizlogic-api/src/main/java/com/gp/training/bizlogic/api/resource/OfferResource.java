package com.gp.training.bizlogic.api.resource;

import com.gp.training.bizlogic.api.model.OfferDTO;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/availability")
public interface OfferResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<OfferDTO> findOffers(@QueryParam("cityId") long cityId,
                              @QueryParam("guestCount") int guestCount,
                              @QueryParam("startDate") String startDate,
                              @QueryParam("endDate") String endDate);

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    OfferDTO get(@PathParam("id") long id);
}
