package com.gp.training.bizlogic.api.builders;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by Antonio on 04.02.2016.
 */
@XmlRootElement(name = "customerBuilder")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerBuilder {

    @XmlElement(name = "id")
    private int id;

    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "surname")
    private String surname;

    @XmlElement(name = "sex")
    private String sexType;

    @XmlElement(name = "birthday")
    private Date birthday;

    @XmlElement(name = "booking_id")
    private long bookingId;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getSexType() {
        return sexType;
    }

    public Date getBirthday() {
        return birthday;
    }

    public long getBookingId() {
        return bookingId;
    }

    public static class Builder {
        private int id;
        private String name;
        private String surname;
        private String sexType;
        private Date birthday;
        private long bookingId;

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder bookingId(int bookingId) {
            this.bookingId = bookingId;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder surname(String surname) {
            this.surname = surname;
            return this;
        }

        public Builder sexType(String sexType) {
            this.sexType = sexType;
            return this;
        }

        public Builder birthday(Date birthday) {
            this.birthday = birthday;
            return this;
        }

        public CustomerBuilder build() {
            CustomerBuilder params = new CustomerBuilder();
            params.id = id;
            params.name = name;
            params.surname = surname;
            params.sexType = sexType;
            params.birthday = birthday;
            params.bookingId = bookingId;

            return params;
        }
    }
}
