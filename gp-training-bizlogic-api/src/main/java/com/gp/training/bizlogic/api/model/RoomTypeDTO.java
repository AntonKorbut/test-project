package com.gp.training.bizlogic.api.model;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "room_type")
@XmlAccessorType(XmlAccessType.FIELD)
public class RoomTypeDTO {

    @XmlAttribute(name = "id")
    private Long id;
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "code")
    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
