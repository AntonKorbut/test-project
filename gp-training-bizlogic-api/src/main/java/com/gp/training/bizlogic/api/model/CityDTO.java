package com.gp.training.bizlogic.api.model;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "city")
@XmlAccessorType(XmlAccessType.FIELD)
public class CityDTO {

    @XmlAttribute(name = "id")
    private Long id;
    @XmlElement(name = "code")
    private String code;
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "country")
    private CountryDTO country;

    public CityDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CountryDTO getCountry() {
        return country;
    }

    public void setCountry(CountryDTO country) {
        this.country = country;
    }
}
