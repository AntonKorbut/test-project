package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.builders.CustomerBuilder;
import com.gp.training.bizlogic.api.model.CustomerDTO;
import com.gp.training.bizlogic.api.resource.CustomerResource;
import com.gp.training.bizlogic.client.ClientSender;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Antonio on 04.02.2016.
 */
@Component
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private ClientSender sender;

    @Override
    public CustomerDTO getCustomer(long id) {
        ResteasyWebTarget webTarget = sender.getRestEasyTarget();
        CustomerResource resource = webTarget.proxy(CustomerResource.class);
        CustomerDTO customerDTO = resource.getCustomer(id);
        webTarget.getResteasyClient().close();
        return customerDTO;
    }


    @Override
    public List<CustomerDTO> createCustomers(List<CustomerBuilder> customerBuilders) {
        ResteasyWebTarget webTarget = sender.getRestEasyTarget();
        CustomerResource customerResource = webTarget.proxy(CustomerResource.class);
        List<CustomerDTO> customerDTOs = customerResource.createCustomers(customerBuilders);
        webTarget.getResteasyClient().close();
        return customerDTOs;
    }
}
