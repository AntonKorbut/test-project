package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.model.CountryDTO;

import java.util.List;

public interface DictionaryService {

    List<CountryDTO> getCountries();

    List<CityDTO> getCities();
}
