package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.builders.BookingBuilder;
import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.resource.BookingResource;
import com.gp.training.bizlogic.client.ClientSender;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BookingServiceImpl implements BookingService {

    @Autowired
    private ClientSender sender;

    @Override
    public BookingDTO createBooking(BookingBuilder bookingBuilder) {
        ResteasyWebTarget webTarget = sender.getRestEasyTarget();
        BookingResource resource = webTarget.proxy(BookingResource.class);
        BookingDTO bookingDTO = resource.createBooking(bookingBuilder);
        webTarget.getResteasyClient().close();
        return bookingDTO;
    }


    @Override
    public BookingDTO getBooking(long id) {
        ResteasyWebTarget webTarget = sender.getRestEasyTarget();
        BookingResource bookingResource = webTarget.proxy(BookingResource.class);
        BookingDTO bookingDTO = bookingResource.getBooking(id);
        webTarget.getResteasyClient().close();
        return bookingDTO;
    }
}
