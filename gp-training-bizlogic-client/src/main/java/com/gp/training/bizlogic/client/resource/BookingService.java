package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.builders.BookingBuilder;
import com.gp.training.bizlogic.api.model.BookingDTO;

public interface BookingService {

    BookingDTO getBooking(long id);

    BookingDTO createBooking(BookingBuilder bookingBuilder);

}
