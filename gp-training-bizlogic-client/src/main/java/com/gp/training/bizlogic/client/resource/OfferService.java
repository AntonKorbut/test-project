package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.model.OfferDTO;

import java.util.List;

public interface OfferService {

    List<OfferDTO> getOffers(long cityId, int guestCount, String startDate, String endDate);

    OfferDTO get(long offerId);
}
