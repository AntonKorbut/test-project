package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.builders.CustomerBuilder;
import com.gp.training.bizlogic.api.model.CustomerDTO;

import java.util.List;

/**
 * Created by Antonio on 04.02.2016.
 */
public interface CustomerService {

    CustomerDTO getCustomer(long id);

    List<CustomerDTO> createCustomers(List<CustomerBuilder> customerBuilders);

}
