create table result_booking
(
  booking_id integer(11) not null,
  customer_id integer(11) not null,
  foreign key (booking_id) references booking(id),
  foreign key (customer_id) references customer (id)
);