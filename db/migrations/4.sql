-- ������� �����
create table country(
	id int(11) primary key  auto_increment, 
	code varchar(10) not null, 
	name varchar(64) not null
	) engine=InnoDB default charset=utf8;

-- ���������� country
insert into country(code, name) values ("BLR", "Belarus"),
	("FRA", "France"), ("ITA", "Italy"); 
	
	
-- ������� �������
create table city(
	id int(11) primary key  auto_increment, 
	code varchar(10) not null, 
	name varchar(64) not null,
	country_id int(11) not null,
	foreign key (country_id) references country(id)
	) engine=InnoDB default charset=utf8;

-- ���������� city
insert into city(code, name, country_id) values 
	("MSQ", "Minsk", 1),
	("GRO", "Grodno", 1), 
	("PAR", "Paris", 2),
	("NIC", "Nice", 2),
	("LQN", "Lyon", 2),
	("ROM", "Rome", 3),
	("MLN", "Milan", 3),
	("NPL", "Neapol", 3);