-- Таблица букингов
create table booking (
	id int(11) primary key auto_increment,
	offer_id INT(11) NOT NULL,
	date_begin DATE NOT NULL,
	date_end DATE NOT NULL,
	guests_count INT(11) NOT NULL,
	foreign key (offer_id) references offer(id)
) engine=InnoDB default charset=utf8;