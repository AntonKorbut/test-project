﻿-- Таблица отелей
create table hotel(
	id int(11) primary key  auto_increment, 
	city_id int(11) not null,
	code varchar(10) not null, 
    name varchar(64) not null, 
	foreign key (city_id) references city(id)
) engine=InnoDB default charset=utf8;

insert into hotel(city_id, code, name)
	values (1, "BHM", "Belarus Hotel"), (1, "VHM", "Victoria Hotel"), (1, "PHM", "President Hotel"),
		   (2, "SHG", "Semashko Hotel"), (2, "NG", "Neman"),
		   (3, "HDLP", "Hotel Du Louvre"), (3, "NPP", "Napoleon Paris"),
		   (4, "BRN", "Beau Rivage"), (4, "HNN", "Hotel Negresco");

-- Таблица предложений
-- Таблица отелей
create table hotel(
	id int(11) primary key  auto_increment, 
	city_id int(11) not null,
	code varchar(10) not null, 
    name varchar(64) not null, 
	foreign key (city_id) references city(id)
) engine=InnoDB default charset=utf8;

insert into hotel(city_id, code, name)
	values (1, "BHM", "Belarus Hotel"), (1, "VHM", "Victoria Hotel"), (1, "PHM", "President Hotel"),
		   (2, "SHG", "Semashko Hotel"), (2, "NG", "Neman"),
		   (3, "HDLP", "Hotel Du Louvre"), (3, "NPP", "Napoleon Paris"),
		   (4, "BRN", "Beau Rivage"), (4, "HNN", "Hotel Negresco");

-- Таблица предложений
CREATE TABLE offer
(
	id INT(11) PRIMARY KEY NOT NULL,
	hotel_id INT(11) NOT NULL,
	room_type_id INT(11) NOT NULL,
	price BIGINT(20) DEFAULT '100' NOT NULL,
	CONSTRAINT offer_ibfk_1 FOREIGN KEY (hotel_id) REFERENCES hotel (id),
	CONSTRAINT offer_ibfk_2 FOREIGN KEY (room_type_id) REFERENCES room_type (id)
);
CREATE INDEX hotel_id ON offer (hotel_id);
CREATE INDEX room_type_id ON offer (room_type_id);

insert into offer(hotel_id, room_type_id)
	select h.id, rt.id  from hotel h join room_type rt;