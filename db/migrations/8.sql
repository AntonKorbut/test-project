CREATE TABLE customer
(
    id INTEGER(11) primary key not null auto_increment,
    name varchar(30) not null,
    surname varchar(30) not null,
    sex varchar(30) not null,
    birthday DATE not null
);

